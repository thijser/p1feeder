package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"time"

	"github.com/mrm/p1feeder/dsmr5p1"
	"github.com/tarm/serial"
)

var device = flag.String("device", "/dev/ttyUSB0", "Serial port device to use")
var baudrate = flag.Int("baud", 115200, "Baud rate to use")

func main() {
	fmt.Println("p1feeder")
	flag.Parse()

	var input io.Reader

	var err error

	c := &serial.Config{Name: *device, Baud: *baudrate}

	input, err = serial.OpenPort(c)
	if err != nil {
		log.Fatal(err)
	}

	ch := dsmr5p1.Poll(input)

	ticker := time.NewTicker(time.Second * 10)
	for range ticker.C {
		t := <-ch
		handleTelegram(t)
	}

	// for t := range ch {
	// 	handleTelegram(t)
	// }

	fmt.Println("exiting")
}

func handleTelegram(t dsmr5p1.Telegram) {
	r, err := t.Parse()
	timestamp := r["0-0:1.0.0"][0]
	ts, err := dsmr5p1.ParseTimestamp(timestamp)

	if err != nil {
		fmt.Println("Error in time parsing:", err)
		return
	}

	fmt.Println("         Timestamp:", ts)
	fmt.Println("     Timestamp raw:", timestamp)
	fmt.Println("Electricty power 1: ", r["1-0:1.7.0"][0])
	fmt.Println("Electricty power 2: ", r["1-0:2.7.0"][0])
	fmt.Println("Electricty total 1: ", r["1-0:1.8.1"][0])
	fmt.Println("Electricty total 2: ", r["1-0:1.8.2"][0])
	fmt.Println("               Gas: ", r["0-1:24.2.1"][1])
	fmt.Println()
}
